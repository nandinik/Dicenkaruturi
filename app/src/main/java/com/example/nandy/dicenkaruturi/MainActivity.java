package com.example.nandy.dicenkaruturi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends AppCompatActivity{
    Button start,reseting, reseting1;
    int random1, random2, count=0, sum,temp;
    ImageView image1, image2;
    TextView result,help;
    String result1,result2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button) findViewById(R.id.button);
        reseting = (Button) findViewById(R.id.button1);
        reseting1 = (Button) findViewById(R.id.button2);
        image1 = (ImageView) findViewById(R.id.imageView3);
        image2 = (ImageView) findViewById(R.id.imageView4);
        result = (TextView) findViewById(R.id.textView2);
        help = (TextView) findViewById(R.id.textView3);
        reseting1.setVisibility(View.GONE);
    }

    public void resetGame(View v)
    {
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_around_center_point);
        image1.startAnimation(animation);
        image2.startAnimation(animation);
        image1.setImageResource(R.drawable.random_dice);
        image2.setImageResource(R.drawable.random_dice);
        count=0;
        result.setText("");
        help.setText("");
        random1=0; random2=0;
        start.setVisibility(View.VISIBLE);
        reseting.setVisibility(View.VISIBLE);
        reseting1.setVisibility(View.GONE);
        start.setText("Play Game");
    }
    public void onPlay(View v)
    {
        count=count+1;
        Random r = new Random();
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_around_center_point);
        image1.startAnimation(animation);
        image2.startAnimation(animation);
        random1 = r.nextInt(6) + 1;
        random2 = r.nextInt(6) + 1;

        if (random1 == 1)
        {
            image1.setImageResource(R.drawable.die1);
        } else if (random1 == 2)
        {
            image1.setImageResource(R.drawable.die2);
        }
        else if (random1 == 3)
        {
            image1.setImageResource(R.drawable.die3);
        }
        else if (random1 == 4)
        {
            image1.setImageResource(R.drawable.die4);
        }
        else if (random1 == 5)
        {
            image1.setImageResource(R.drawable.die5);
        }
        else
            image1.setImageResource(R.drawable.die6);


        if (random2 == 1)
        {
            image2.setImageResource(R.drawable.die1);
        }
        else if (random2 == 2)
        {
            image2.setImageResource(R.drawable.die2);
        }
        else if (random2 == 3)
        {
            image2.setImageResource(R.drawable.die3);
        }
        else if (random2 == 4)
        {
            image2.setImageResource(R.drawable.die4);
        }
        else if (random2 == 5)
        {
            image2.setImageResource(R.drawable.die5);
        }
        else
            image2.setImageResource(R.drawable.die6);

        sum=random1+random2;

        if(count==1)
        {
            if(sum==7 || sum==11)
            {
                result.setText("You Won");
                help.setText("To start a new game again click on reset button");
                start.setVisibility(View.GONE);
                reseting.setVisibility(View.GONE);
                reseting1.setVisibility(View.VISIBLE);

            }
            else if(sum==2 || sum==3 || sum==12)
            {
                result.setText("You Losed");
                help.setText("To start a new game again click on reset button");
                start.setVisibility(View.GONE);
                reseting.setVisibility(View.GONE);
                reseting1.setVisibility(View.VISIBLE);

            }
            else if(sum==4 || sum==5 || sum==6 || sum==8 || sum==9 || sum==10)
            {
                temp=sum;
                result1 = Integer.toString(sum);
                result2 = "Player points" + " : " + result1;
                start.setText("Play Again");
                result.setText(result2);
                help.setText("To continue game Click on play again");
            }
            else
            {

            }
        }
        else if(temp!=0)
        {
            start.setText("Play Again");
            if(sum==7)
            {
                result.setText("You Losed");
                help.setText("To start a  new game again click on reset button");
                start.setVisibility(View.GONE);
                reseting.setVisibility(View.GONE);
                reseting1.setVisibility(View.VISIBLE);
            }
            else if(temp==sum)
            {
                result.setText("You Won");
                help.setText("To start a new game again click on reset button");
                start.setVisibility(View.GONE);
                reseting.setVisibility(View.GONE);
                reseting1.setVisibility(View.VISIBLE);
            }
            else
            {
               result.setText("Ponits Not Matched");
                help.setText("To continue game click on play again");
            }
        }
        else
        {
            Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_around_center_point);
            image1.startAnimation(animation1);
            image2.startAnimation(animation1);
            image1.setImageResource(R.drawable.random_dice);
            image2.setImageResource(R.drawable.random_dice);
            result.setText("");
            help.setText("Please click on reset button and click on play game to start a new game");
        }
    }
}
